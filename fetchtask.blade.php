@extends("master")
@section('fetchtask')
    {{--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">--}}
    {{--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>--}}
    {{--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>--}}

    <section class="login_section layout_padding">
        <div class="container">
            <div class="row">
                {{session()->get('message')}}
            </div>
            <div class="col-md-6">
                <div class="login_form">
                    <h5>
                        Update Task

                    </h5>
                    <form action="{{url('user/'.$update->id)}}" method="post">
                        @csrf
                        @method('PUT')
                        <div>
                            <input type="text" id="taskname" name="taskname" placeholder="Taskname" value="{{$update->taskname}}" />

                        </div>
                        <div>
                            {{--<textarea class="form-control" id="description" name="description"  placeholder="Description" value="{{$update->description}}"></textarea>--}}
                            <input type="text" id="description" name="description" placeholder="Description " value="{{$update->description}}" />

                        </div>

                        <div>
                            <input type="text" id="duration" name="duration" placeholder="Duration " value="{{$update->duration}}" />

                        </div>
                        <div>
                            <input type="text" id="createby" name="createby" placeholder="created by " value="{{$update->createby}}" />

                        </div>
                        <div>
                            <input type="text" id="assignto" name="assignto" placeholder="assign to " value="{{$update->assignto}}"/>

                        </div>
                        <button type="submit">Update</button>

                    </form>

                </div>
            </div>
        </div>
        </div>
    </section>
    @endsection

