@extends('master')
@section('task')
    {{--@livewireStyles--}}
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <section class="login_section layout_padding">
        <div class="container">
            <div class="row">
                {{session()->get('message')}}
            </div>
            <div class="col-md-6">
                <div class="login_form">
                    <h5>
                        Task

                    </h5>
                    <form action="{{url('user')}}" method="post">
                        @csrf

                        <div>
                            <input type="text" id="taskname" name="taskname" placeholder="Taskname" />

                        </div>
                        <div>
                            {{--<input type="text" id="description" name="description" placeholder="Description " />--}}
                            <textarea class="form-control" id="description" name="description"  placeholder="Description"></textarea>
                        </div>
                        <div>
                            <input type="text" id="duration" name="duration" placeholder="Duration " />

                        </div>
                        <div>
                            <input type="text" id="createby" name="createby" placeholder="created by " />

                        </div>
                        <div>
                            <input type="text" id="assignto" name="assignto" placeholder="assign to " />

                        </div>
                        <button type="submit">Save</button>

                    </form>

                </div>
            </div>
        </div>
        </div>
    </section>



    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.23/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.7/css/responsive.dataTables.min.css"/>

    <div class="main-container">
        <main class="pd-ltr-10 xs-pd-10-10">
            <div class="min-height-200px">
                <div class="page-header">
                    <div class="row">
                        <div class="col-md-6 col-sm-6" style="margin-left: 400px;">
                            <div class="pd-30 card-box mb-30">
                                {{--<a href="{{url('task')}}/task">--}}
                                    {{--<i class="btn btn-outline-secondary" style="margin-left: 20px"> + Add Product</i>--}}
                                {{--</a>--}}
                                <br><br>

                                <div class="pd-20 card-box mb-30">
                                    <div class="clearfix mb-20">
                                        <div class="pull-left">
                                            <h4 class="text-blue h4">task</h4>
                                        </div>
                                        <span style="color:#1b00ff;margin-left: 60px;text-decoration:underline"> {{session()->get('message')}}</span>

                                        <table class="table responsive" id="ttable">
                                            <thead>
                                            <tr>
                                                <th class="all">id</th>
                                                <th class="all">taskname</th>
                                                <th class="none">description</th>
                                                <th class="all">duration</th>
                                                <th class="all" >createby</th>
                                                <th class="all">assignto</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @php  $i=1; @endphp
                                            @foreach($task as $val)
                                                {{--$image=\DB::table('product_images')->where('p_id',$val->p_id)->first();--}}
                                                {{--@endphp--}}
                                                <tr>
                                                    <td>{{$i++}}</td>
                                                    {{--<td><img src="{{asset('Assets')}}/img/product/{{$image->url}}" width="80px"></td>--}}
                                                    <td>{{$val->taskname}}</td>
                                                    <td>{{$val->description}}</td>
                                                    <td>{{$val->duration}}</td>
                                                    <td>{{$val->createby}}</td>
                                                    <td>{{$val->assignto}}</td>
                                                    <td>

                                                        {{--<a  href="#"><i class="dw dw-eye"></i> </a>&nbsp;--}}

                                                        <a  href="{{url('user/'.$val->id.'/edit')}}"><span class="glyphicon glyphicon-pencil"></span> </a>
                                                        @method('DELETE')
                                                        <a  href="{{url('user/'.$val->id)}}"><span class="glyphicon glyphicon-trash"></span></a>
                                                        {{--<a  href=""><span class="glyphicon glyphicon-trash"></span></a>--}}
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                        <a href="{{url('task')}}">
                                            <i class="btn btn-outline-secondary" style="margin-left: 600px"> + Add Task</i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        </div>

@endsection
@section('script')
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.23/datatables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.7/js/dataTables.responsive.min.js"></script>
    <script>
        $(document).ready(function(){
            $('#ttable').DataTable();
        });
    </script>
    {{--@livewireScripts--}}
@endsection

